/* Copyright 1995 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* wimp.c
 *
 * Author: Keith Ruttle (Acorn)
 *
 * Description
 * ===========
 * Code for !Gateway dummy RISC OS desktop application
 *
 * Environment
 * ===========
 * Acorn RISC OS 3.11 or later.
 *
 * Compiler
 * ========
 * Acorn Archimedes C release 5.06 or later.
 *
 * Change record
 * =============
 *
 * JPD  Jem Davies (Cambridge Systems Design)
 *
 *
 * 10-Oct-95  12:53  JPD  Version 1.00
 * First version with change record. Attempted to make compile without
 * warnings.
 *
 *
 **End of change record*
 */

#include <stdlib.h>
#include <string.h>
#include "kernel.h"
#include "swis.h"

#include "wimp.h"
#include "wimpt.h"
#include "win.h"
#include "event.h"
#include "res.h"
#include "resspr.h"
#include "menu.h"
#include "template.h"
#include "msgs.h"
#include "dbox.h"
#include "werr.h"
#include "sprite.h"
#include "bbc.h"
#include "baricon.h"

#define gateway_menu_info 1

static menu gateway_menu;
static int is_ip_gateway = 0;
static char icon_text[16] = { 0 };
static wimp_i icon = 0;

/******************************************************************************/

static void gateway_info_about_program(void)
{
   dbox  d;

   if (d = dbox_new("ProgInfo"), d != NULL)
   {
      dbox_setfield(d, 4, msgs_lookup("_Version"));
      dbox_show(d);
      dbox_fillin(d);
      dbox_dispose(&d);
   }
}

/******************************************************************************/

static os_error *sprite_read_os_size(char *sprite, int *width, int *height)
{
/*
 * returns the OS unit width and height of a sprite
 * in the wimp's sprite area
 */

   os_error *err;
   os_regset r;

   r.r[0] = 40;
   r.r[2] = (int)sprite;
   err = wimp_spriteop_full(&r);

   if (err)
      return err;

   *width = r.r[3] << bbc_modevar(r.r[6], bbc_XEigFactor);
   *height = r.r[4] << bbc_modevar(r.r[6], bbc_YEigFactor);

   return NULL;
}

/******************************************************************************/

static os_error *baricon_init(wimp_icreate *new_icreate, wimp_w window,
                                                       char *sprite, char *text)
{
   os_error *err;
   int sprite_base;
   int swidth;
   int sheight;

   new_icreate->w = window;
   new_icreate->i.box.x0 = 0;
   new_icreate->i.box.y0 = -16;
   sprite_base = 20;
   err = sprite_read_os_size(sprite + 1, &swidth, &sheight);

   if (err)
       return err;

   new_icreate->i.box.y1 = sprite_base + sheight;

   if (strlen(text) * 16 > swidth)
      new_icreate->i.box.x1 = strlen(text) * 16;
   else
      new_icreate->i.box.x1 = swidth;

   new_icreate->i.flags = wimp_ITEXT | wimp_ISPRITE | wimp_IHCENTRE |
                 wimp_INDIRECT |
                (wimp_IBTYPE * wimp_BCLICKDEBOUNCE) |
                (wimp_IFORECOL * 7) |
                (wimp_IBACKCOL * 1);
   new_icreate->i.data.indirecttext.buffer = text;
   new_icreate->i.data.indirecttext.validstring = sprite;
   new_icreate->i.data.indirecttext.bufflen = strlen( text ) + 1;

   return NULL;
}

/******************************************************************************/

static os_error *baricon_add(char *sprite, char *text)
{
   os_error *err;
   wimp_icreate new_icreate;

   err = baricon_init( &new_icreate, -2, sprite, text);
   if (err)
      return err;
   err = wimp_create_icon (&new_icreate, &icon);
   if (err)
      return err;
   win_activeinc();

   return NULL;
}

/******************************************************************************/

static void dummy_event_handler(wimp_eventstr *e, void *handle)
{
   e = e;
   handle = handle;
}

/******************************************************************************/

static void gateway_menuproc(void *handle, char *hit)
{
   handle = handle;

   switch (hit[0])
   {

      case gateway_menu_info:
         gateway_info_about_program();
         break;

      default:
         break;
   }
}

/******************************************************************************/

static BOOL gateway_initialise(void)
{
   wimpt_init("Gateway");
   res_init("Gateway");
   resspr_init();
   template_init();
   dbox_init();
   msgs_init();
   event_setmask((wimp_emask)0);

   if (gateway_menu = menu_new(msgs_lookup("title:Gateway"),
                               msgs_lookup("menu:>Info")), gateway_menu == NULL)
   {
      werr(0, "Failed to create menu");
      return FALSE;
   }

   is_ip_gateway = 1;

   strcpy(icon_text, msgs_lookup("title:Gateway"));
   if ( wimpt_complain(baricon_add("S!Gateway", icon_text)) != 0)
   {
      werr(0, "Failed to get an icon on the icon bar");
      return FALSE;
   }

   win_register_event_handler(win_ICONBAR, dummy_event_handler, NULL);

   if (!event_attachmenu(win_ICONBAR, gateway_menu, gateway_menuproc, 0))
   {
      werr(0, "Failed to attach menu");
      return FALSE;
   }

   return TRUE;
}

/******************************************************************************/

static int do_cli(char *str)
{
   _kernel_swi_regs r;

   r.r[0] = (int)str;
   (void) _kernel_swi(XOS_Bit | OS_CLI, &r, &r);

   return 0;
}

/******************************************************************************/

static void unset_govar(void)
{
   do_cli("unset GateWay$Running");
}

/******************************************************************************/

int main(void)
{
   if (gateway_initialise())
   {
      atexit(unset_govar);
      while (TRUE)
         event_process();
   }

   return 0;
}

/******************************************************************************/

/* EOF wimp.c */
